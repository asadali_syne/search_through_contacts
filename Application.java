package com.asad.stc.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.asad.stc.beans.Contacts;

/**
 * 
 * @author ASAD ALI
 * 
 *
 */
public class Application {
	private static Application application = null;
	private static Logger reportLog = Logger.getLogger("report");
	private static Logger errorLog = Logger.getLogger("error");
	private BufferedReader bufferedReader;

	public Application(BufferedReader bufferedReader) {
		this.bufferedReader = bufferedReader;
	}

	/*
	 * As per requirement contact list name should not contain special
	 * characters, white spaces in middle of names are allowed but not as whole
	 * name. i.e. only white space(s) can't be added to contact list.
	 */
	public boolean isValidName(String input) {
		Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
		Matcher m = p.matcher(input);
		boolean flag = true;
		if (!(input.trim().length() > 0)) {
			reportLog.warn("Invalid Name, Name as only White Spaces are not allowed! Try Again");
			return !flag;
		} else if (m.find()) {
			reportLog.warn("Invalid Name, Special Symbole's are not allowed! Try Again");
			return !flag;
		}
		return flag;
	}

	/*
	 * Search a name if found then add that name to map and return to caller, if
	 * exact match is found initialize/set the key of map as zero for that
	 * name., so that it could displayed at top
	 */
	public static Map<Integer, String> searchContacts(String input, List<Contacts> contactList) {
		Map<Integer, String> seacrchedContacts = new HashMap<Integer, String>();
		if (contactList.size() != 0) {
			for (int i = 0; i < contactList.size(); i++) {
				/*
				 * if exact match then add the searched name at first position,
				 * here used map to do so with key value pointing at zero(0).
				 */
				if (contactList.get(i).getContact().toLowerCase().equalsIgnoreCase(input.toLowerCase())) {
					seacrchedContacts.put(0, contactList.get(i).getContact());
					/*
					 * Searched string may be a sub string and in that case
					 * order does't matter.
					 */
				} else if (contactList.get(i).getContact().toLowerCase().contains(input.toLowerCase())) {
					seacrchedContacts.put(i + 1, contactList.get(i).getContact());
				}

			}

		}

		return seacrchedContacts;

	}

	/*
	 * if a user enters wrong input that is other than 1, 2 and 3. System will
	 * throw a warning message, and will ask to Re-Enter.
	 */
	public String validateInput(String input) {
		if (!(input.equalsIgnoreCase("1") || input.equalsIgnoreCase("2") || input.equalsIgnoreCase("3"))) {
			reportLog.warn("Wrong input \n Re-Enter either of 1, 2, 3");
			try {
				input = bufferedReader.readLine().toString().trim();
				validateInput(input);
			} catch (IOException e) {
				errorLog.error(Level.ERROR, e);
			}
		}
		return input;

	}

	public static Application getInstance(BufferedReader bufferedReader) {
		if (application == null)
			return new Application(bufferedReader);
		return application;
	}

}
