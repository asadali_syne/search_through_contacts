# Search_Through_Contacts
Search through Contacts

 Developer - Asad Ali
 
 Technology – Java(1.7)
 
 IDE – Eclipse
 
 Release Version- 1.0
 
Goal – 

To implement Contacts application which supports following two operations, 

 Add contact 

 Search Contact

Contact is defined as firstName and/or lastName separated by space. Some examples of valid
contacts are: 

 Chris Harris 

 Chris Cairns 

 Harry Potter 

 Chris

 Add contact should take the name and add it to contact list so that it becomes searchable.

 Search contact should match the contacts for a given search string.

Expected Program Run:

1) Add contact 2) Search 3) Exit 

1 

Enter name: Chris Harris 

1) Add contact 2) Search 3) Exit 

1 

Enter name: Chris 

1) Add contact 2) Search 3) Exit 

2 

Enter name: Ch 

Chris Harris Chris 

1) Add contact 2) Search 3) Exit 

2 

Enter name: Chris 

Chris 

Chris Harris 

1) Add contact 2) Search 3) Exit 

3 

Happy Searching

Note:-

1) If you rank exact matches higher than other matches i.e. in the above run when we
searched for Chris, Chris was printed before Chris Harris

2) If you allow searching by last name, in the above example If Ha is searched then Chris
Harris should be the printed. 

3) If exact match is not there for the searched string then order doesn’t matter.
Configuration /Set up:- 

Follow below steps to configure the workspace and get code executed.

 Step - 1
 
Open IDE  import repository/workspace as a Java Project and add below jars into the
custom library/Referenced library (if no custom Library found in workspace).

 log4j.jar – For Log Implementation

Step – 2

 Add/configure log4j.properties to src/resources folder if not found in workspace.

Step – 3

Execute the code as Java Application, output will be displayed onto the console as well
as, two log file will be generated as report.log/error.log under logs folder.

Note -

 Search String is case insensitive, i.e. if ha is searched the Chris Harris will be printed.

 Names containing special symbols can’t be added or searched. However digits are

allowed.
 White spaces are allowed only in between names(first & last), only white spaces are not
allowed

Here are few things that, I kept in mind before submitting code to ensure that the code focuses
on attributes that the coding principle looks like.

 Behaviour of an object distinguished from its state and the state is encapsulated. 

 Applied SOLID principles to the code.

 Cared about YAGNI and KISS (additional info here)

 Followed OCP (Open/Closed) Principle.

 Followed Singleton pattern.

 Logging Implementations.

 Handled user-input errors in your code.

