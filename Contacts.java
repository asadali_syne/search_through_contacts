package com.asad.stc.beans;

/**
 * 
 * @author ASAD ALI
 * 
 *         Contacts is a POJO, to store contacts, Here we are adding contacts
 *         through constructor injection. However setter injection can also be
 *         implemented
 *
 */
public class Contacts {
	private String contact;

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public Contacts(String contact) {
		this.contact = contact;
	}

	@Override
	public String toString() {
		return "Contacts [contacts=" + contact + ", getContacts()=" + getContact() + "]";
	}

}
