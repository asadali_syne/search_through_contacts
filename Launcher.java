package com.asad.stc.launch;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.asad.stc.sync.Sync;
import com.asad.stc.sync.SyncContacts;
import com.asad.stc.util.Application;

/**
 * 
 * @author ASAD ALI
 */
/**
 * Contacts application which supports following two operations: Add contact,
 * Search Contact A message will be printed to the console for input. Here it
 * looks like 1)Add contact 2) Search 3) Exit, Based on inputs actions will be
 * performed
 * 
 * 
 */

public class Launcher {
	private static BufferedReader bufferedReader = null;
	private static StringBuilder stringBuilder = null;
	private static Logger reportLog = Logger.getLogger("report");
	private static Logger errorLog = Logger.getLogger("error");
	private static Sync syncContacts = null;
	private static Application application = null;

	/*
	 * Singleton design pattern has been implemented, for the Application and
	 * SyncContact class. This process will be static(compile time) loading.
	 */
	static {
		bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		stringBuilder = new StringBuilder();
		stringBuilder.append("1) Add contact ");
		stringBuilder.append("2) Search ");
		stringBuilder.append("3) Exit ");
		application = Application.getInstance(bufferedReader);
		syncContacts = SyncContacts.getInstance(bufferedReader, stringBuilder);
	}

	/*
	 * System will read input through Standard Input, Input must be either of 1,
	 * 2, 3 . else system will ask to re enter input.
	 * 
	 */

	public static void main(String[] args) {
		reportLog.info(stringBuilder);
		try {
			/*
			 * validateInput() method of Application class will validate inputs.
			 */
			String input = application.validateInput(bufferedReader.readLine().toString().trim());
			/*
			 * syncContact() method is factory method which will sync contacts
			 * with the contact list. Basically it will add contacts to the
			 * list.
			 */
			syncContacts.syncContact(input);
		} catch (IOException | ClassCastException e) {
			errorLog.error(Level.INFO, e);
		}
		System.exit(0);
	}

}
