package com.asad.stc.sync;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.asad.stc.beans.Contacts;
import com.asad.stc.util.Application;

/**
 * 
 * @author ASAD ALI */
/**
 * Reusable methods of Application class i.e. isvalidName(), searchContact() and
 * validateInput() will be invoked as per requirements. factory Design pattern
 * has been implemented. here syncContact() is a factory method which will
 * perform addition of contact to the list and retrieval if name exists.
 * 
 *
 */
public class SyncContacts extends Application implements Sync {
	private BufferedReader bufferedReader;
	private StringBuilder stringBuilder;
	private static SyncContacts syncContacts;
	private static Logger reportLog = Logger.getLogger("report");
	private static Logger errorLog = Logger.getLogger("error");

	SyncContacts(BufferedReader bufferedReader, StringBuilder stringBuilder) {
		super(bufferedReader);
		this.bufferedReader = bufferedReader;
		this.stringBuilder = stringBuilder;

	}

	public static SyncContacts getInstance(BufferedReader bufferedReader, StringBuilder stringBuilder) {
		if (syncContacts == null)
			return new SyncContacts(bufferedReader, stringBuilder);
		return syncContacts;
	}

	/*
	 * syncContact() will add/search names to the contact based on inputs.
	 * Validation part will be taken care by Application class
	 */
	@Override
	public void syncContact(String input) {
		List<Contacts> contactList = new ArrayList<Contacts>();
		for (; input.equalsIgnoreCase("1") || input.equalsIgnoreCase("2") || input.equalsIgnoreCase("3");) {
			switch (input) {
			case "1":
				reportLog.info("Enter name: ");
				try {
					input = bufferedReader.readLine().toString().trim();
					/*
					 * Check if user input is valid then add, special symbol and
					 * only white spaces are not allowed as part of name
					 */
					if (isValidName(input))
						contactList.add(new Contacts(input));
					reportLog.debug(contactList);
					reportLog.info(stringBuilder);
					/*
					 * validateInput() a reusable method, will verify for the
					 * user inputs
					 */
					input = validateInput(bufferedReader.readLine().toString().trim());
				} catch (IOException | NullPointerException e) {
					errorLog.error(Level.ERROR, e);
				}
				break;
			case "2":
				reportLog.info("Enter name: ");
				try {
					input = bufferedReader.readLine().toString();
					/*
					 * Check if user input is valid then search, special symbol
					 * and only white spaces are not allowed as part of name
					 */
					if (isValidName(input) && contactList.size() != 0) {
						Map<Integer, String> contacts = searchContacts(input.trim(), contactList);
						if (contacts.size() != 0) {
							for (String contact : contacts.values()) {
								reportLog.info(contact);

							}
						} else {
							reportLog.info("No Match Found in Contact List");
						}
					} else {
						reportLog.info("Contact List is Empty, Kindly add contacts");
					}
					reportLog.info(stringBuilder);
					input = validateInput(bufferedReader.readLine().toString().trim());

				} catch (IOException | NullPointerException e) {

					errorLog.error(Level.ERROR, e);
				}
				break;
			case "3":
				reportLog.info("Happy Searching ");
				System.exit(0);
				break;
			default:
				reportLog.info("Unknown Command");
			}
		}

	}

}
